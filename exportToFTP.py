import urllib2, urllib, zlib, hmac, hashlib, time, json, os, sys, ftplib, argparse
from ftplib import FTP
from xml.dom import minidom

ROOT_URL = 'http://services.uplynk.com'
OWNER = 'ACCOUNT GUID' # CHANGE THIS TO YOUR ACCOUNT GUID
SECRET = 'API KEY' # CHANGE THIS TO YOUR SECRET API KEY
#API Call to Integration services
def Call(uri, **msg):
    msg['_owner'] = OWNER
    msg['_timestamp'] = int(time.time())
    msg = json.dumps(msg)
    msg = zlib.compress(msg, 9).encode('base64').strip()
    sig = hmac.new(SECRET, msg, hashlib.sha256).hexdigest()
    body = urllib.urlencode(dict(msg=msg, sig=sig))
    return json.loads(urllib2.urlopen(ROOT_URL + uri, body).read())


def epoch_minutes(n):
    return int((time.time() - (n * 86400)) * 1000)


#print Asset List
def printList():    
    myList = Call('/api2/asset/list', order='created')
    asset_info = []
    dict = {}
    for i, asset in enumerate(myList["assets"]):
        ID = asset['id']
        dateCreated = asset['created']
        dict['aID'] = ID
        asset_info.append(dict)
        new_id = asset_info[0]['aID']
        if int(dateCreated) >= int(epoch_minutes(6)):
            for j, aID in enumerate(new_id):
                myAssestInfo = Call('/api2/asset/get', id=new_id)
                desc = (myAssestInfo['asset']['desc'])
                job = (myAssestInfo['asset']['job_type'])
                url = (myAssestInfo['asset']['test_players'][0]['url'])
                mp4 = (myAssestInfo['asset']['mp4_url'])
                ttml = (myAssestInfo['asset']['ttml_url'])
                thumb = (myAssestInfo)['asset']['thumb_url']

                meta_desc = (myAssestInfo)['asset']['meta']['Description']
                meta_guid = (myAssestInfo)['asset']['meta']['guid']
                meta_categories = (myAssestInfo)['asset']['meta']['Category']
                meta_tags = (myAssestInfo)['asset']['meta']['Tags']

                myPath = ID
                fullfilename = os.path.join(myPath, new_id)
                if not os.path.exists(new_id): os.makedirs(new_id)
                mp4_filename = fullfilename + '.mp4'
                thumb_filename = fullfilename + '.jpg'

                #Pull Down Assets to local folder
                urllib.urlretrieve(mp4, filename=mp4_filename)
                urllib.urlretrieve(thumb, filename=thumb_filename)    
                #urllib.urlretrieve(ttml, filename='ttml_' + ttml + '.ttml')

                

                doc = minidom.Document()

                rss = doc.createElement('rss')
                doc.appendChild(rss)


                channel = doc.createElement('channel')
                rss.appendChild(channel)

                title = doc.createElement('title')
                channel.appendChild(title)

                link = doc.createElement('link')
                channel.appendChild(link)

                desc = doc.createElement('description')
                channel.appendChild(desc)

                item = doc.createElement('item')
                channel.appendChild(item)


                pubDate = doc.createElement('date')
                pubDate_text = doc.createTextNode(str(dateCreated))
                pubDate.appendChild(pubDate_text)
                item.appendChild(pubDate)

                guid = doc.createElement('guid')
                guid_text = doc.createTextNode(str(meta_guid))
                guid.appendChild(guid_text)
                item.appendChild(guid)

                title2 = doc.createElement('title')
                title2_text = doc.createTextNode('Title will go here')
                title2.appendChild(title2_text)
                item.appendChild(title2)

                mc = doc.createElement("media:content")
                mc.setAttribute('type', 'video/mp4')
                mc.setAttribute('url', 'file://' + str(mp4_filename))
                item.appendChild(mc)

                md = doc.createElement("media:description")
                md.setAttribute('type', 'plain')
                md_text = doc.createTextNode(str(meta_desc))
                md.appendChild(md_text)
                mc.appendChild(md)

                mt = doc.createElement("media:thumbnail")
                mt.setAttribute('url', 'file://' + str(thumb_filename))
                mc.appendChild(mt)

                mk = doc.createElement("media:keywords")
                mk_text = doc.createTextNode(str(meta_tags))
                mk.appendChild(mk_text)
                mc.appendChild(mk)

                mc2 = doc.createElement("media:content")
                mc2.setAttribute('type', 'application/ttml+xml')
                mc2.setAttribute('medium', 'document')
                mc2.setAttribute('url', 'url to ttml')
                item.appendChild(mc2)

                xml_str = doc.toprettyxml(indent="  ")
                with open(fullfilename + ".xml", "w") as f:
                    f.write(xml_str)


                

                #export via FTP
                mp4_upload = new_id + '.mp4'
                thumb_upload = new_id + '.jpg'
                xml_upload = new_id + '.xml'
                xml_filename = fullfilename + ".xml"
                ftp = FTP('') #Enter FTP Address
                ftp.login('USER', 'PASSWORD') #USER = Username, PASS = Password
                ftp.cwd('PATH ON REMOTE SERVER') #add path to remote directory for upload
                ftp.storbinary('STOR '+mp4_upload, open(mp4_filename, 'rb'))
                ftp.storbinary('STOR '+thumb_upload, open(thumb_filename, 'rb'))
                ftp.storbinary('STOR '+xml_upload, open(xml_filename, 'rb'))
                ftp.quit()
                break
printList()